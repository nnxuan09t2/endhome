<?php
namespace Admin\Test\TestCase\Model\Table;

use Admin\Model\Table\HomesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * Admin\Model\Table\HomesTable Test Case
 */
class HomesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Admin\Model\Table\HomesTable
     */
    public $Homes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.admin.homes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Homes') ? [] : ['className' => 'Admin\Model\Table\HomesTable'];
        $this->Homes = TableRegistry::get('Homes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Homes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
