/**
 * Created by NgocXuan on 2016/12/07.
 */
(function(window){
    CKEDITOR.replace( 'ck_content',
        {
            filebrowserBrowseUrl: '<?= $this->request->webroot ?>js/ckfinder/ckfinder.html',
            filebrowserImageBrowseUrl: '<?= $this->request->webroot ?>js/ckfinder/ckfinder.html?type=Images',
            filebrowserUploadUrl: '<?= $this->request->webroot ?>js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
            filebrowserImageUploadUrl: '<?= $this->request->webroot ?>js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images'
        });
    CKEDITOR.config.language = '<?= $lang ?>';
}(window))();

