$(document).ready(function(){
    $('#menu-top .dropdown').dropdown({
        transition:'slide down',
        on: 'hover'
    });
    $('.ui.menu.sidebar.icon .item').popup({
        position:'top right'
    });
    $('#sidebar-menu').click(function(){
        $(this).find('i').toggleClass('unlock lock');
        $('.ui.sidebar').toggleClass('icon');
        $('#container-admin').toggleClass('translate72');
        $('.ui.sidebar .item').each(function(index,element){
            $(element).find('label').toggleClass('hidden');
        });
        if (typeof(Storage)!=='undefined'){
            localStorage['sidebar_class'] = $('aside').attr('class');
            localStorage['label_class'] = $('.ui.sidebar .item label').eq(0).attr('class');
        }
    })
    if(localStorage['sidebar_class'].indexOf('icon')>0){
        $('#container-admin').addClass('translate72');
    }
})