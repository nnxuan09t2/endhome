<?php
namespace Admin\Controller;

use Admin\Controller\AppController;
use Cake\Filesystem\Folder;
use Cake\Core\Plugin;

/**
 * Homes Controller
 *
 * @property \Admin\Model\Table\HomesTable $Homes
 */

class HomesController extends AppController
{
    public function test(){
        if($this->request->is('ajax')){
            $this->viewBuilder()->layout('ajax');
            $test = 'dasdasd';
            $this->set(compact('test'));
            $this->render('Element/home/template/introduce');
        }
    }
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $admin_plugin = Plugin::path('Admin');
        $templateFolder = new Folder($admin_plugin.'src/Template/Element/home/template');
        $templates = $templateFolder->find('.*\.ctp', true);
        $listTemplate = [];
        foreach ($templates as $template){
            $value = substr($template,0,strrpos($template,'.'));
            $listTemplate[$value] = $value;
        }
        $homes = $this->paginate($this->Homes);
        $this->set(compact('homes','listTemplate'));
        $this->set('_serialize', ['homes']);
    }

    /**
     * View method
     *
     * @param string|null $id Home id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $home = $this->Homes->get($id, [
            'contain' => []
        ]);

        $this->set('home', $home);
        $this->set('_serialize', ['home']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $home = $this->Homes->newEntity();
        if ($this->request->is('post')) {
            $home = $this->Homes->patchEntity($home, $this->request->data);
            if ($this->Homes->save($home)) {
                $this->Flash->success(__('The home has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The home could not be saved. Please, try again.'));
            }
        }
        $templateFolder = new Folder(APP.'/Template/Element/home');
        $templates = $templateFolder->find('.*\.ctp', true);
        foreach ($templates as $template){
            $value = substr($template,0,strrpos($template,'.'));
            $listTemplate[$value] = $value;
        }
        $this->set(compact('home','listTemplate'));
        $this->set('_serialize', ['home']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Home id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $home = $this->Homes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $home = $this->Homes->patchEntity($home, $this->request->data);
            if ($this->Homes->save($home)) {
                $this->Flash->success(__('The home has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The home could not be saved. Please, try again.'));
            }
        }
        $templateFolder = new Folder(APP.'/Template/Element/home');
        $templates = $templateFolder->find('.*\.ctp', true);
        foreach ($templates as $template){
            $value = substr($template,0,strrpos($template,'.'));
            $listTemplate[$value] = $value;
        }
        $this->set(compact('home','listTemplate'));
        $this->set('_serialize', ['home','listTemplat']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Home id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $home = $this->Homes->get($id);
        if ($this->Homes->delete($home)) {
            $this->Flash->success(__('The home has been deleted.'));
        } else {
            $this->Flash->error(__('The home could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
