<!DOCTYPE html>
<html>
    <head>
        <?php echo $this->Html->charset(); ?>
        <title>
            <?php echo $this->fetch('title'); ?>
        </title>

        <?php
            echo $this->Html->meta('icon');
            echo $this->Html->css(['base','cake','semantic.min','sidebar','Admin.admin']);
            echo $this->Html->script(['jquery.min','semantic.min','lib/jquery.address','ckeditor/ckeditor','ckfinder/ckfinder','Admin.admin']);
            echo $this->fetch('meta');
            echo $this->fetch('css');
            echo $this->fetch('script');
        ?>
    </head>
    <body>
        <header>
            <?= $this->element('frame/menu-top',['lang'=>$lang]) ?>
        </header>
        <!--  Sidebar Element  -->
        <?= $this->element('frame/sidebar')?>
        <!-- End Sidebar -->
        <div id="container-admin" class="pusher" style="padding-top:20px;">
            <?php echo $this->Flash->render(); ?>
            <?php echo $this->fetch('content'); ?>
        </div>
        <footer>
        </footer>
        <script>
            CKEDITOR.config.language = '<?= $lang ?>';
        </script>
    </body>
</html>
