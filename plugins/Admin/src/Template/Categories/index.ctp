<div class="categories index large-12 medium-12 columns content">
    <h3><?= __('Categories') ?></h3>
    <table cellpadding="0" cellspacing="0" class="ui table">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('name_en',__('Menu name')) ?></th>
                <th scope="col"><?= $this->Paginator->sort('name_vn') ?></th>
                <th scope="col"><?= $this->Paginator->sort('link') ?></th>
                <th scope="col"><?= $this->Paginator->sort('menu_top',__('Top')) ?></th>
                <th scope="col"><?= $this->Paginator->sort('menu_sidebar',__('Sidebar')) ?></th>
                <th scope="col"><?= $this->Paginator->sort('menu_nav',__('Navigation')) ?></th>
                <th scope="col" class="actions"><?= __('Order') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($categories as $category): ?>
            <tr>
                <td><?= h(str_repeat('|-',$category->level).$category->name_en) ?></td>
                <td><?= h($category->name_vn) ?></td>
                <td><?= h($category->link) ?></td>
                <td><?= h($category->menu_top) ?></td>
                <td><?= h($category->menu_sidebar) ?></td>
                <td><?= h($category->menu_nav) ?></td>
                <td>
                    <?= $this->Form->postLink(__('<i class="icon arrow up"></i>'),['action'=>'moveUp',$category->id],['confirm'=>__('Are you sure you want to move up # {0}?', $category->id),'escape'=>false]) ?>
                    <?= $this->Form->postLink(__('<i class="icon arrow down"></i>'),['action'=>'moveDown',$category->id],['confirm'=>__('Are you sure you want to move down # {0}?', $category->id),'escape'=>false]) ?>
                </td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $category->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $category->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $category->id], ['confirm' => __('Are you sure you want to delete # {0}?', $category->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
