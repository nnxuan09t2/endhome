<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Section'), ['action' => 'edit', $section->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Section'), ['action' => 'delete', $section->id], ['confirm' => __('Are you sure you want to delete # {0}?', $section->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Sections'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Section'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Parent Sections'), ['controller' => 'Sections', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Parent Section'), ['controller' => 'Sections', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Services'), ['controller' => 'Services', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Service'), ['controller' => 'Services', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="sections view large-9 medium-8 columns content">
    <h3><?= h($section->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Parent Section') ?></th>
            <td><?= $section->has('parent_section') ? $this->Html->link($section->parent_section->name, ['controller' => 'Sections', 'action' => 'view', $section->parent_section->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Title') ?></th>
            <td><?= h($section->title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Link') ?></th>
            <td><?= h($section->link) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Avatar') ?></th>
            <td><?= h($section->avatar) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __(' Background') ?></th>
            <td><?= h($section->_background) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __(' Style') ?></th>
            <td><?= h($section->_style) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __(' Class') ?></th>
            <td><?= h($section->_class) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __(' Id') ?></th>
            <td><?= h($section->_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Type') ?></th>
            <td><?= h($section->type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($section->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Level') ?></th>
            <td><?= $this->Number->format($section->level) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __(' Height') ?></th>
            <td><?= $this->Number->format($section->_height) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __(' Width') ?></th>
            <td><?= $this->Number->format($section->_width) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __(' Column') ?></th>
            <td><?= $this->Number->format($section->_column) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Lft') ?></th>
            <td><?= $this->Number->format($section->lft) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Rght') ?></th>
            <td><?= $this->Number->format($section->rght) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($section->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($section->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Locate') ?></h4>
        <?= $this->Text->autoParagraph(h($section->locate)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Sections') ?></h4>
        <?php if (!empty($section->child_sections)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Parent Id') ?></th>
                <th scope="col"><?= __('Title') ?></th>
                <th scope="col"><?= __('Link') ?></th>
                <th scope="col"><?= __('Level') ?></th>
                <th scope="col"><?= __('Avatar') ?></th>
                <th scope="col"><?= __(' Background') ?></th>
                <th scope="col"><?= __(' Style') ?></th>
                <th scope="col"><?= __(' Class') ?></th>
                <th scope="col"><?= __(' Id') ?></th>
                <th scope="col"><?= __(' Height') ?></th>
                <th scope="col"><?= __(' Width') ?></th>
                <th scope="col"><?= __(' Column') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Lft') ?></th>
                <th scope="col"><?= __('Rght') ?></th>
                <th scope="col"><?= __('Type') ?></th>
                <th scope="col"><?= __('Locate') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($section->child_sections as $childSections): ?>
            <tr>
                <td><?= h($childSections->id) ?></td>
                <td><?= h($childSections->parent_id) ?></td>
                <td><?= h($childSections->title) ?></td>
                <td><?= h($childSections->link) ?></td>
                <td><?= h($childSections->level) ?></td>
                <td><?= h($childSections->avatar) ?></td>
                <td><?= h($childSections->_background) ?></td>
                <td><?= h($childSections->_style) ?></td>
                <td><?= h($childSections->_class) ?></td>
                <td><?= h($childSections->_id) ?></td>
                <td><?= h($childSections->_height) ?></td>
                <td><?= h($childSections->_width) ?></td>
                <td><?= h($childSections->_column) ?></td>
                <td><?= h($childSections->created) ?></td>
                <td><?= h($childSections->modified) ?></td>
                <td><?= h($childSections->lft) ?></td>
                <td><?= h($childSections->rght) ?></td>
                <td><?= h($childSections->type) ?></td>
                <td><?= h($childSections->locate) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Sections', 'action' => 'view', $childSections->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Sections', 'action' => 'edit', $childSections->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Sections', 'action' => 'delete', $childSections->id], ['confirm' => __('Are you sure you want to delete # {0}?', $childSections->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Services') ?></h4>
        <?php if (!empty($section->services)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Category Id') ?></th>
                <th scope="col"><?= __('Title') ?></th>
                <th scope="col"><?= __('Description') ?></th>
                <th scope="col"><?= __('Avatar') ?></th>
                <th scope="col"><?= __('Link') ?></th>
                <th scope="col"><?= __('Section Id') ?></th>
                <th scope="col"><?= __('Meta') ?></th>
                <th scope="col"><?= __('Options') ?></th>
                <th scope="col"><?= __('Locate') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($section->services as $services): ?>
            <tr>
                <td><?= h($services->id) ?></td>
                <td><?= h($services->category_id) ?></td>
                <td><?= h($services->title) ?></td>
                <td><?= h($services->description) ?></td>
                <td><?= h($services->avatar) ?></td>
                <td><?= h($services->link) ?></td>
                <td><?= h($services->section_id) ?></td>
                <td><?= h($services->meta) ?></td>
                <td><?= h($services->options) ?></td>
                <td><?= h($services->locate) ?></td>
                <td><?= h($services->created) ?></td>
                <td><?= h($services->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Services', 'action' => 'view', $services->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Services', 'action' => 'edit', $services->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Services', 'action' => 'delete', $services->id], ['confirm' => __('Are you sure you want to delete # {0}?', $services->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
