<?= $this->Form->create($section,['class'=>'ui form']) ?>
    <fieldset>
        <legend><?= __('Add Section') ?></legend>
        <?php
        echo '<div class="ui two columns grid">';
        echo '<div class="column">';
        echo $this->Form->input('parent_id', ['options' => $parentSections, 'empty' => __('select section parent')]);
        echo $this->Form->input('name');
        echo $this->Form->input('title');
        echo $this->Form->input('link');
        echo $this->Form->input('position');
        echo $this->Form->input('_id',['type'=>'text','label'=>__('Id')]);
        echo $this->Form->input('_height');
        echo $this->Form->input('_width');
        echo $this->Form->input('_background',['type'=>'color','value'=>'#ffffff']);
        echo $this->Form->input('_class',['id'=>'abc']);
        echo '</div>';
        echo '<div class="column">';
        echo $this->Form->input('_column');
        echo $this->Form->input('type',['options'=>['list'=>__('List'),'block'=>__('Block')]]);
        echo $this->Form->input('avatar',['type'=>'hidden','id'=>'avatar']);
        echo '<div class="ui image floated right" id="preview" style="border: 2px dotted dimgrey;margin-top: 10px;width: 100%; height: 400px;">';
        if (!empty($section->avatar)){
            echo '<img src="'.$section->avatar.'" class="ui image large" style="height:395px;width:100%" />';
        }
        echo '</div>';
        echo '</div>';
        echo '</div>';
        echo '<div class="clearfix"></div>';
        echo $this->Form->input('content',['class'=>'ckeditor','id'=>'ck_content']);
        echo $this->Form->input('_style',['type'=>'textarea']);
        ?>
        <?= $this->Form->button(__('Submit'),['class'=>'ui button primary floated left','style'=>'margin-top:20px;']) ?>
    </fieldset>
<?= $this->Form->end() ?>
<script>
    $('#preview').click(function(e){
        e.preventDefault();
        selectFileWithCKFinder('avatar');
    })
    function selectFileWithCKFinder( elementId ) {
        CKFinder.modal( {
            chooseFiles: true,
            width: 1000,
            height: 600,
            onInit: function( finder ) {
                finder.on( 'files:choose', function( evt ) {
                    var file = evt.data.files.first();
                    var output = document.getElementById( elementId );
                    output.value = file.getUrl();
                    var image_holder = $("#preview");
                    image_holder.empty();
                    $("<img />", {
                        "src": file.getUrl(),
                        "class": "ui image large",
                        'style': 'height: 395px;width:100%'
                    }).appendTo(image_holder);
                    image_holder.show(1000);
                } );
                finder.on( 'file:choose:resizedImage', function( evt ) {
                    var output = document.getElementById( elementId );
                    output.value = evt.data.resizedUrl;
                } );
            }
        } );
    }
    CKEDITOR.replace( 'ck_content',
        {
            filebrowserBrowseUrl: '<?= $this->request->webroot ?>js/ckfinder/ckfinder.html',
            filebrowserImageBrowseUrl: '<?= $this->request->webroot ?>js/ckfinder/ckfinder.html?type=Images',
            filebrowserUploadUrl: '<?= $this->request->webroot ?>js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
            filebrowserImageUploadUrl: '<?= $this->request->webroot ?>js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images'
        });
</script>
