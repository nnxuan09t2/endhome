<nav class="large-3 medium-4 columns" >
    <div class="ui vertical fluid tabular menu" id="actions-sidebar">
        <?php
            if(count($homes)>0){
                $flag = false;
                foreach ($homes as $home){
                    if(!$flag){
                        echo '<a class="item active" data-tab="'.$home->link.'"><i class="icon '.$home->icon.'"></i>'.$home->name.'</a>';
                        $flag = true;
                    }else {
                        echo '<a class="item" data-tab="'.$home->link.'"><i class="icon '.$home->icon.'"></i>'. $home->name.'</a>';
                    }
                }
                echo '<a class="item" data-tab="add_new"><i class="plus icon"></i>'.__('Add new icon').'</a>';
            }else {
                echo '<a class="item active"  data-tab="add_new"><i class="plus icon"></i>'.__('Add new icon').'</a>';
            }
        ?>
    </div>
</nav>
<div class="homes index large-9 medium-8 columns content">
    <?php
        if(count($homes)){
            $flag = false;
            foreach ($homes as $home){
                if(!$flag){
                    echo '<div class="ui active tab" data-tab="'.$home->link.'">'.$this->element('home/_form',compact('home','listTemplate')).'</div>';
                    $flag = true;
                }else {
                    echo '<div class="ui tab" data-tab="'.$home->link.'">'.$this->element('home/_form',compact('home','listTemplate')).'</div>';
                }
            }
            $home = \Cake\ORM\TableRegistry::get('Homes')->newEntity();
            echo '<div class="ui tab" data-tab="add_new">'.$this->element('home/_form',compact('home','listTemplate')).'</div>';
        }else {
            $home = \Cake\ORM\TableRegistry::get('Homes')->newEntity();
            echo '<div class="ui active tab" data-tab="add_new">'.$this->element('home/_form',compact('home','listTemplate')).'</div>';
        }
    ?>
</div>
<script>
    $('.menu .item').tab({
        history: true,
        historyType: 'hash'
    });
</script>
