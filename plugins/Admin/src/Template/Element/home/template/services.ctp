<div clas="container" id="cnt-service">
    <div class="ui menu borderless">
        <a class="active item" data-tab="first"><?= __('Khách hàng') ?></a>
        <a class="item" data-tab="second"><?= __('Bảo trị hệ thống MEP') ?></a>
        <a class="item" data-tab="third"><?= __('Thiết kế hệ thống MEP') ?></a>
    </div>
    <div class="ui active tab" data-tab="first" data-url="admin/homes/test">
    </div>
    <div class="ui tab" data-tab="second">
    </div>
    <div class="ui tab " data-tab="third">
    </div>
</div>
<script>
    $(window).on('load',function(){
        var url = $('.tab.active').data('url');
        $.get(url,function(res){
            $('.tab.active').html(res);
        })
    });
    $('.menu .item').tab({
        auto: true,
        path: 'admin/homes/test?p={tab}',
        evaluateScripts : 'once'
    });
</script>