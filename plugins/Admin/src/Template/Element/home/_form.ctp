<?php
    $action = isset($home->id)?'edit':'add';
?>
<?= $this->Form->create($home, [
    'url' => ['controller' => 'homes', 'action' => $action]
]); ?>
    <fieldset>
        <legend><?= __('Add Home') ?></legend>
        <?php
        echo $this->Form->input('name');
        echo $this->Form->input('title');
        echo $this->Form->input('content',['class'=>'ckeditor','id'=>'content_'.$home->id,'style'=>'height: 500px;']);
        echo $this->Form->input('options',['options'=>$listTemplate]);
        echo $this->Form->input('icon');
        echo $this->Form->input('type',['options'=>[__('Text'),__('Block'),__('Slide')]]);
        echo $this->Form->input('link');
        echo $this->Form->button(__('Submit'),['class'=>'ui button floated left primary','style'=>'margin-top: 10px;']);
        ?>
    </fieldset>
<?= $this->Form->end() ?>
<script>
    CKEDITOR.replace( 'content_<?= $home->id?>',
        {
            filebrowserBrowseUrl: '<?= $this->request->webroot ?>js/ckfinder/ckfinder.html',
            filebrowserImageBrowseUrl: '<?= $this->request->webroot ?>js/ckfinder/ckfinder.html?type=Images',
            filebrowserUploadUrl: '<?= $this->request->webroot ?>js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
            filebrowserImageUploadUrl: '<?= $this->request->webroot ?>js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
            filebrowserWindowWidth: '1000',
            filebrowserWindowHeight: '700'
        });

</script>
