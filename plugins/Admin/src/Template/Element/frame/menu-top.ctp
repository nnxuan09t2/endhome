<div class="ui menu mini fixed top" id="menu-top" style="margin-bottom: 0;border-radius: 0;" >
    <a class="item" id="sidebar-menu">
        <i class="icon unlock"></i>
    </a>
    <?= $this->Html->link(
            $this->Html->image('logo-64.png',['class'=>'image mini']),
            [
                'plugin'=>false,
                'controller'=>'pages',
                'action'=>'display',
                'home'
            ],
            [
                'class'=>'item',
                'escape'=>false
            ]
        );
    ?>
    <a href="" class="item">
        <?php echo __('career');?>
    </a>
    <a href="" class="item">
        <?php echo __('introduce'); ?>
    </a>
    <a href="" class="item">
        <?php echo __('contact'); ?>
    </a>
    <div class="menu floated right">
        <div class="ui dropdown item simple">
            <?php if($lang==='vi'):?>
                <i class="vietnam flag"></i>
                Tiếng việt
            <?php else:?>
                <i class="canada flag"></i>
                English
            <?php endif; ?>
            <i class="dropdown icon"></i>
            <div class="menu">
                <?php
                echo $this->Html->link('<i class="canada flag"></i> '.__('english'),
                    array(
                        'plugin'=>false,
                        'controller'=>'settings',
                        'action'=>'language',
                        'en'
                    ),
                    array(
                        'class'=>'item',
                        'escape'=>false
                    )
                );
                echo $this->Html->link('<i class="vietnam flag"></i> '.__('vietnamese'),
                    array(
                        'plugin'=>false,
                        'controller'=>'settings',
                        'action'=>'language',
                        'vi'
                    ),
                    array(
                        'class'=>'item',
                        'escape'=>false
                    )
                );
                ?>
            </div>
        </div>
        <a href="" class="item">
            <i class="tasks icon"></i>Cộng việc
        </a>
        <a href="" class="item">
            <i class="alarm icon"></i>Thông báo
        </a>
        <div class="ui item dropdown">
            <img src="img/avatar.jpg" class="circular image" style="height: 20px;width: 20px;margin-right: 5px;">
            <a class="header middle aligned" href="">
                Nguyễn Ngọc Xuân
                <i class="dropdown icon"></i>
            </a>
            <div class="menu">
                <a class="item label">
                    <i class="rocket icon"></i>
                    Applications
                </a>
                <a class="item">International Students</a>
                <?php 
                    echo $this->Html->link('<i class="power icon"></i> Logout',array('controller'=>'users','action'=>'logout'),array('class'=>'item', 'escape'=>false));
                 ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">


</script>