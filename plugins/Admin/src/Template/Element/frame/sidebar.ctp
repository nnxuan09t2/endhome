<aside>
    <?php
        echo $this->Html->link('<i class="block layout icon"></i><label>'.__('Home').'</label>',
            [
                'plugin'=>'admin',
                'controller'=>'categories',
                'action'=>'index'
            ],
            [
                'class'=>'item',
                'escape'=>false
            ]
        );
        echo $this->Html->link('<i class="plus icon"></i><label>'.__('Add Categories').'</label>',
            [
                'plugin'=>'admin',
                'controller'=>'categories',
                'action'=>'add'
            ],
            [
                'class'=>'item',
                'escape'=>false
            ]
        );
        echo $this->Html->link('<i class="block layout icon"></i><label>'.__('Sections').'</label>',
            [
                'plugin'=>'admin',
                'controller'=>'sections',
                'action'=>'index'
            ],
            [
                'class'=>'item',
                'escape'=>false
            ]
        );
        echo $this->Html->link('<i class="plus icon"></i><label>'.__('Sections').'</label>',
            [
                'plugin'=>'admin',
                'controller'=>'sections',
                'action'=>'add'
            ],
            [
                'class'=>'item',
                'escape'=>false
            ]
        );
        echo $this->Html->link('<i class="home icon"></i><label>'.__('Sections').'</label>',
            [
                'plugin'=>'admin',
                'controller'=>'homes',
                'action'=>'index'
            ],
            [
                'class'=>'item',
                'escape'=>false
            ]
        );
    ?>
    <a class="item">
        <i class="block layout icon"></i>
        <label>Home</label>
    </a>
    <a class="item">
        <i class="smile icon"></i>
        <label>Home</label>
    </a>
    <a class="item">
        <i class="calendar icon"></i>
        <label>Home</label>
    </a>
</aside>
<script>
    (function(window){
        if(typeof(Storage)!=='undefined' && localStorage['sidebar_class']!='undefined'){
            $('aside').attr('class',localStorage['sidebar_class']);
            $('.ui.sidebar .item label').attr('class',localStorage['label_class']);
        }else {
            $('aside').attr('class','ui left inline vertical sidebar menu visible');
        }
    })(window);
</script>
