<?php
namespace Admin\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Homes Model
 *
 * @method \Admin\Model\Entity\Home get($primaryKey, $options = [])
 * @method \Admin\Model\Entity\Home newEntity($data = null, array $options = [])
 * @method \Admin\Model\Entity\Home[] newEntities(array $data, array $options = [])
 * @method \Admin\Model\Entity\Home|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Admin\Model\Entity\Home patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Admin\Model\Entity\Home[] patchEntities($entities, array $data, array $options = [])
 * @method \Admin\Model\Entity\Home findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class HomesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('homes');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('name');

        $validator
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        $validator
            ->allowEmpty('content');

        $validator
            ->allowEmpty('options');

        $validator
            ->allowEmpty('type');

        $validator
            ->allowEmpty('locate');

        $validator
            ->allowEmpty('link');

        $validator
            ->allowEmpty('icon');

        return $validator;
    }
}
