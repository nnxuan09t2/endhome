<?php
namespace Admin\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Sections Model
 *
 * @property \Cake\ORM\Association\BelongsTo $ParentSections
 * @property \Cake\ORM\Association\HasMany $ChildSections
 * @property \Cake\ORM\Association\HasMany $Services
 *
 * @method \Admin\Model\Entity\Section get($primaryKey, $options = [])
 * @method \Admin\Model\Entity\Section newEntity($data = null, array $options = [])
 * @method \Admin\Model\Entity\Section[] newEntities(array $data, array $options = [])
 * @method \Admin\Model\Entity\Section|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Admin\Model\Entity\Section patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Admin\Model\Entity\Section[] patchEntities($entities, array $data, array $options = [])
 * @method \Admin\Model\Entity\Section findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 * @mixin \Cake\ORM\Behavior\TreeBehavior
 */
class SectionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('sections');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Tree');

        $this->belongsTo('ParentSections', [
            'className' => 'Admin.Sections',
            'foreignKey' => 'parent_id'
        ]);
        $this->hasMany('ChildSections', [
            'className' => 'Admin.Sections',
            'foreignKey' => 'parent_id'
        ]);
        $this->hasMany('Services', [
            'foreignKey' => 'section_id',
            'className' => 'Admin.Services'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('position');

        $validator
            ->allowEmpty('title');

        $validator
            ->allowEmpty('content');

        $validator
            ->allowEmpty('link');

        $validator
            ->integer('level')
            ->allowEmpty('level');

        $validator
            ->allowEmpty('avatar');

        $validator
            ->allowEmpty('_background');

        $validator
            ->allowEmpty('_style');

        $validator
            ->allowEmpty('_class');

        $validator
            ->allowEmpty('_id');

        $validator
            ->allowEmpty('_height');

        $validator
            ->allowEmpty('_width');

        $validator
            ->allowEmpty('_column');

        $validator
            ->requirePresence('type', 'create')
            ->notEmpty('type');

        $validator
            ->allowEmpty('locate');

        $validator
            ->allowEmpty('name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['parent_id'], 'ParentSections'));

        return $rules;
    }
}
