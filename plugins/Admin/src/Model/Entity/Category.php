<?php
namespace Admin\Model\Entity;

use Cake\ORM\Entity;

/**
 * Category Entity
 *
 * @property int $id
 * @property int $parent_id
 * @property int $lft
 * @property int $rght
 * @property string $name_en
 * @property string $name_vn
 * @property bool $menu_top
 * @property bool $menu_sidebar
 * @property bool $menu_nav
 *
 * @property \Admin\Model\Entity\ParentCategory $parent_category
 * @property \Admin\Model\Entity\ChildCategory[] $child_categories
 */
class Category extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
