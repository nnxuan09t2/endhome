<?php
namespace Admin\Model\Entity;

use Cake\ORM\Entity;

/**
 * Section Entity
 *
 * @property int $id
 * @property string $position
 * @property int $parent_id
 * @property string $title
 * @property string $content
 * @property string $link
 * @property int $level
 * @property string $avatar
 * @property string $_background
 * @property string $_style
 * @property string $_class
 * @property string $_id
 * @property string $_height
 * @property string $_width
 * @property string $_column
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property int $lft
 * @property int $rght
 * @property string $type
 * @property string $locate
 * @property string $name
 *
 * @property \App\Model\Entity\Section $parent_section
 * @property \App\Model\Entity\Section[] $child_sections
 * @property \App\Model\Entity\Service[] $services
 */
class Section extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
