<?php
/**
 * Created by PhpStorm.
 * User: Ba Xa Fa Xinh
 * Date: 11/13/2016
 * Time: 12:16 AM
 */
namespace App\Controller;

class SettingsController extends AppController {
    public function language($lang){
        $this->Cookie->write('Config.language',$lang);
        return $this->redirect($this->referer());
    }
}