<div class="ui menu teal sticky large banner borderless overlay" role="navigation" id="main-menu">
    <a href="" class="image item floated left" style="padding: 5px;">
        <img src="img/logo-64.png" class="logo" style="margin-right: .5em;height: 64px;width: 64px;" alt="CMS Comany">
        <b>CMS Co.,LTD</b>
    </a>
    <?php
    if (!empty($categories)) {
        $arr_categories = $categories->toArray();
        foreach ($categories as $key => $category) {
            if ($category->level == 0) {
                $parent_id = $category->id;
                $child = array_filter($arr_categories, function ($element) use ($parent_id) {
                    return $element->parent_id == $parent_id;
                });
                if (count($child) > 0) {
                    echo '<div class="ui item dropdown">';
                    echo $category->name_vn . '<i class="dropdown icon"></i>';
                    echo '<div class="menu">';
                    foreach ($child as $key => $value) {
                        echo $this->Html->link($value->name_vn, $value->link, ['class' => 'item']);
                    }
                    echo '</div>';
                    echo '</div>';
                } else {
                    echo $this->Html->link($category->name_vn, $category->link, ['class' => 'item']);
                }
            }
        }
    }
    ?>
    <div class="item ui search">
        <div class="ui icon input">
            <input class="prompt" type="text" placeholder="Tìm kiếm dịch vụ...">
            <i class="search icon"></i>
        </div>
        <div class="results"></div>
    </div>
</div>
<script>
    var flag = true;
    $(window).scroll(function(e){
        var scroll_top = $(this).scrollTop();
        if(scroll_top>60){
            $('#sidebar-menu').show();
            $('#main-menu').addClass('fixed top top30 inverted');
            if(flag){
                $('#main-menu').transition('fade up','1000ms');
                flag = false;
            }
            $('#menu-top').addClass('fixed top');
            $('#menu-top').removeClass('inverted');
        }else{
            $('#sidebar-menu').hide();
            if(!flag){
                $('#main-menu').transition('slide down','1000ms');
                flag = true;
            }
            $('#main-menu').removeClass('fixed top top30 inverted');
            $('#menu-top').addClass('inverted');
            $('#menu-top').removeClass('fixed top');
        }
    })
    // $('.ui.sticky').sticky({
    //     // context: '#main-menu'
    //     offsetBottom: 0
    // })
    $('#main-menu .dropdown').dropdown({
        transition:'slide down',
        on: 'hover'
    });
</script>