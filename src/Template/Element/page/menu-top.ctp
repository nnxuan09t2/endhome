<div class="ui menu small inverted" id="menu-top" style="margin-bottom: 0;border-radius: 0;" >
	<a class="item" id="sidebar-menu">
		<i class="icon sidebar"></i>
		<?= __('All Catagories')?>
	</a>

	<a href="" class="item">
		<i class="icon home"></i>
	</a>
	<a href="" class="item">
		<i class="browser icon"></i>
		<?php echo __('introduce');?>
	</a>
	<div href="" class="item ui dropdown">
		<i class="configure icon"></i>
		Đội ngũ kỷ thuật
		<i class="icon dropdown"></i>
		<div class="menu">
			<a href="" class="item"> Nguồn nhân lực</a>
			<a href="" class="item"> Phương tiện kỹ thuật hỗ trợ</a>
		</div>
	</div>
	<a href="" class="item">
		<i class="newspaper icon"></i>
		<?php echo __('news');?>
	</a>
	<a href="" class="item">
		<i class="add user icon"></i>
		<?php echo __('career');?>
	</a>
	<a href="" class="item">
		<i class="send icon"></i>
		<?php echo __('contact');?>
	</a>
	<div class="menu floated right">
		<?php 
			echo $this->Html->link('<i class="user icon"></i> '.__('Đăng nhập'),array('controller'=>'users','action'=>'login'),array('class'=>'item', 'escape'=>false));
		?>
		<a href="" class="item">
			<i class="registered icon"></i>Đăng ký
		</a>
		<div class="ui item dropdown">
			<i class="help circle outline icon"></i>
			<i class="dropdown icon"></i>
			<div class="menu">
				<a class="item label">
					<i class="idea icon"></i>
					Tư vấn dịch vụ
				</a>
				<a class="item">
					<i class="call icon"></i>
					Tư vấn online
				</a>
				<a class="item">
					<i class="configure icon"></i>
					Tư vấn cài đặt
				</a>
			</div>
		</div>
		<div class="ui dropdown item simple">
			<?php if($lang==='vi'):?>
				<i class="vietnam flag"></i>
				Tiếng việt
			<?php else:?>
				<i class="canada flag"></i>
				English
			<?php endif; ?>
			<i class="dropdown icon"></i>
			<div class="menu">
				<?php
				echo $this->Html->link('<i class="canada flag"></i> '.__('english'),
					array(
						'controller'=>'settings',
						'action'=>'language',
						'en'
					),
					array(
						'class'=>'item',
						'escape'=>false
					)
				);
				echo $this->Html->link('<i class="vietnam flag"></i> '.__('vietnamese'),
					array(
						'controller'=>'settings',
						'action'=>'language',
						'vi'
					),
					array(
						'class'=>'item',
						'escape'=>false
					)
				);
				?>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('#sidebar-menu').hide();
	$('#menu-top .dropdown').dropdown({
		transition:'slide down',
		on: 'hover'
	});
	$('#sidebar-menu').click(function(){
//		$(this).find('i').toggleClass('sidebar close');
		$('#main-menu')
			.transition({
				transition: 'slide down',
				duration : '1000ms',
				onShow: function(){
					$('#sidebar-menu i').eq(0).removeClass('sidebar');
					$('#sidebar-menu i').eq(0).addClass('close');
				},
				onHide: function(){
					$('#sidebar-menu i').eq(0).removeClass('close');
					$('#sidebar-menu i').eq(0).addClass('sidebar');
				}
			})
//			.sidebar('toggle');
	})
//    $('.ui.sidebar')
//        .first()
//        .sidebar('attach events', '#sidebar-menu', 'show');

</script>