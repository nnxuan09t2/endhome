<i class="remove circle outline icon large"></i>
<?php 
	echo $this->Html->link('<i class="tasks icon"></i>'.__('Catalogue Manager'),
		[
			'plugin'=>'admin',
			'controller'=>'categories',
			'action'=>'index'
		],
		[
			'class'=>'item',
			'escape'=>false
		]
	);
?>
<a class="item">
    <i class="block layout icon"></i>
    Topics
</a>
<a class="item">
    <i class="smile icon"></i>
    Friends
</a>
<a class="item">
    <i class="calendar icon"></i>
    History
</a>
<script type="text/javascript">
	$('.remove')
		.on('click', function() {
			$(this).closest('.sidebar').sidebar('hide');
		}
	);
</script>