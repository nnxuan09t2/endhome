<nav id="c-menu--slide-right" class="ui menu">
    <div class="item">
        <a href="">
            <img src="<?php echo $this->webroot?>img/logo.png" alt="logo">
        </a>
    </div>
    <?php
    foreach ($catalogues as $key=>$value){
        if ($value[1]=='0'){
            echo $this->Html->link('</i>'.$value[0],'/',array('class'=>'item','escape'=> false));
        }

    }
    ?>
    <a class="item right" id="menu-sidebar">
        <i class="sidebar icon"></i>
    </a>
</nav>
<div id="settings">
    <div id="social">
        <a href="#">
            <i class="facebook f icon"></i>
        </a>
        <a href="#">
            <i class="youtube icon"></i>
        </a>
        <a href="#">
            <i class="google plus icon"></i>
        </a>
        <a href="#">
            <i class="twitter icon"></i>
        </a>
        <a href="#">
            <i class="connectdevelop icon"></i>
        </a>

    </div>
    <div id="lang">
        <div class="menu selectiodropdown">
            <a href="/trungviet/change/language/vn" class="item">
                <i class="vietnam flag"></i>
            </a>
            <a href="/trungviet/change/language/eng" class="item">
                <i class="canada flag"></i>
            </a>
        </div>
        
        <!-- <a href="/trungviet/change/language/vn" id="vn_lang">
            <i class="vietnam flag" ></i>
        </a>
        <a href="/trungviet/change/language/eng" id="vn_lang">
            <i class="canada flag" id="en_lang"></i>
        </a> -->
    </div>
    <i class="setting icon" id="icon-setting"></i>
</div>
<?php echo $this->Html->script(array('jquery.min','semantic.min')); ?>
<script>
    (function(window){
        $('.dropdown.selection').dropdown();
    })(window);
</script>