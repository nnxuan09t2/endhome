<div class="ui item" id="gioi-thieu">
	<h4 class="header">GIỚI THIỆU VÀ DỰ ÁN THỰC HIỆN</h4>	
	<div class="ui two column grid">
		<div class="column">
			 <!-- <img src="img/logo-64.png" class="tiny centered ui image"> -->
			<h5 class="center aligned header">
				CANADA MAINTAEIN SERVICES
			</h5>
			<div class="content">
				<img src="img/logo-128.png" class="small left floated ui image">
				<div class="description">
					Te eum doming eirmod, nominati pertinacia argumentum ad his. Ex eam alia facete scriptorem, est autem aliquip detraxit at. Usu ocurreret referrentur at, cu epicurei appellantur vix. Cum ea laoreet recteque electram, eos choro alterum definiebas in. Vim dolorum definiebas an. Mei ex natum rebum iisque. Ex eam alia facete scriptorem, est autem aliquip detraxit at. Usu ocurreret referrentur at, cu epicurei appellantur vix. Cum ea laoreet recteque electram, eos choro alterum definiebas in. Vim dolorum definiebas an. Mei ex natum rebum iisque.
				</div>
				
			</div>
		</div>
		<div class="column">
			<div class="controller">
		<button href="" class="ui icon transparent large button left">
			<i class="chevron left icon large"></i>
		</button>
		<button href="" class="ui icon transparent large button right">
			<i class="chevron right icon large"></i>
		</button>
		</div>			
			<div class="ui content shape">
			<div class="sides">
	    		<div class="side">
		      		<div class="ui fluid card borderless">
		      		<div class="content">
		      			<a href="" class="header">DỰ ÁN ĐÃ THỰC HIỆN</a>
		      		</div>
		        		<div class="image">
		         		 <img src="img/img2.jpg" class="ui image medium">
		        </div>
		        <div class="content">
		          <div class="header">Steve Jobes</div>
		          <div class="meta">
		            <a>Acquaintances</a>
		          </div>
		          <div class="description">
		            Steve Jobes is a fictional character designed to resemble someone familiar to readers.
		          </div>
		        </div>
						<div class="ui two bottom attached buttons">
							<div class="ui button">
								<i class="add icon"></i>
								Queue
							</div>
							<div class="ui primary button">
								<i class="play icon"></i>
								Watch
							</div>
						</div>
		      </div>
		    </div>
		    <div class="side">
		      		<div class="ui fluid card">
		      		<div class="content">
		      			<a href="" class="header">DỰ ÁN ĐÃ THỰC HIỆN</a>
		      		</div>
		        		<div class="image">
		         		 <img src="img/img1.jpg" class="ui image medium">
		        </div>
		        <div class="content">
		          <div class="header">Steve Jobes</div>
		          <div class="meta">
		            <a>Acquaintances</a>
		          </div>
		          <div class="description">
		            Steve Jobes is a fictional character designed to resemble someone familiar to readers.
		          </div>
		        </div>
		        <div class="extra content">
		          <span class="right floated">
		            Joined in 2014
		          </span>
		          <span>
		            <i class="user icon"></i>
		            151 Friends
		          </span>
		        </div>
		      </div>
		    </div>
    			<div class="side active">
      				<div class="ui fluid card">
      				<div class="content">
		      			<a href="" class="header">DỰ ÁN ĐÃ THỰC HIỆN</a>
		      		</div>
						<div class="image">
	          				<img src="img/img3.jpg" class="ui image medium">
	        		</div>
	        		<div class="content">
	          			<a class="header">Stevie Feliciano</a>
	          			<div class="meta">
	            			<span class="date">Joined in 2014</span>
	          			</div>
	          			<div class="description">
	            			Stevie Feliciano is a library scientist living in New York City. She likes to spend her time reading, running, and writing.
	          			</div>
	        		</div>
	        		<div class="extra content">
	          			<a>
	            			<i class="user icon"></i>
	            			22 Friends
	          			</a>
	        		</div>
	      		</div>
    		</div>
  		</div>
  			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$('.controller button.left').click(function(){
	$('.shape').shape('flip over');
});
$('.controller button.right').click(function(){
	$('.shape').shape('flip back');
});
</script>