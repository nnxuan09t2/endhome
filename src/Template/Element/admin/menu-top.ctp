<div class="ui menu mini inverted fixed top" id="menu-top" style="margin-bottom: 0;border-radius: 0;" >
    <a class="item" id="sidebar-menu">
        <i class="icon unlock"></i>
    </a>

    <a href="" class="item">
        <i class="icon home"></i>
    </a>
    <a href="" class="item">
        <?php echo __('career');?>
    </a>
    <a href="" class="item">
        <?php echo __('introduce'); ?>
    </a>
    <a href="" class="item">
        <?php echo __('contact'); ?>
    </a>
    <div class="menu floated right">
        <div class="ui dropdown item simple">
            <?php if($lang==='vn'):?>
                <i class="vietnam flag"></i>
                Tiếng việt
            <?php else:?>
                <i class="canada flag"></i>
                English
            <?php endif; ?>
            <i class="dropdown icon"></i>
            <div class="menu">
                <?php
                echo $this->Html->link('<i class="canada flag"></i> '.__('english'),
                    array(
                        'controller'=>'settings',
                        'action'=>'language',
                        'eng'
                    ),
                    array(
                        'class'=>'item',
                        'escape'=>false
                    )
                );
                echo $this->Html->link('<i class="vietnam flag"></i> '.__('vietnamese'),
                    array(
                        'controller'=>'settings',
                        'action'=>'language',
                        'vn'
                    ),
                    array(
                        'class'=>'item',
                        'escape'=>false
                    )
                );
                ?>
            </div>
        </div>
        <a href="" class="item">
            <i class="tasks icon"></i>Cộng việc
        </a>
        <a href="" class="item">
            <i class="alarm icon"></i>Thông báo
        </a>
        <div class="ui item dropdown">
            <img src="<?php echo $this->webroot?>img/avatar.jpg" class="circular image" style="height: 20px;width: 20px;margin-right: 5px;">
            <a class="header middle aligned" href="">
                Nguyễn Ngọc Xuân
                <i class="dropdown icon"></i>
            </a>
            <div class="menu">
                <a class="item label">
                    <i class="rocket icon"></i>
                    Applications
                </a>
                <a class="item">International Students</a>
                <?php 
                    echo $this->Html->link('<i class="power icon"></i> Logout',array('controller'=>'users','action'=>'logout'),array('class'=>'item', 'escape'=>false));
                 ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#menu-top .dropdown').dropdown({
        transition:'slide down',
        on: 'hover'
    });
    $('.ui.menu.sidebar.icon .item').popup({
        position:'top right'
    });
    $('#sidebar-menu').click(function(){
        $(this).find('i').toggleClass('unlock lock');
        $('.ui.sidebar').toggleClass('icon');
        $('.pusher').toggleClass('translate72');
        $('.ui.sidebar .item').each(function(index,element){
            $(element).find('label').toggleClass('hidden');
            $(element).find('img').toggleClass('sidebar-mini');
        });
    })

</script>