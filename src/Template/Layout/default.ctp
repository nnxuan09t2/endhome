<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $this->fetch('title'); ?>
	</title>
	<?php
		echo $this->Html->meta('icon');
		echo $this->Html->css(array('semantic.min','animate','glide.core.min','glide.theme.min','menu','slide','home'));
		echo $this->Html->script(array('jquery.min','semantic.min','jquery.glide.min'));
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>
<body>
	<header>
		<?php echo $this->element('page/menu-top',['lang'=>$lang]); ?>
		<?php echo $this->element('page/header-page',['categories'=>$categories])?>
	</header>
	<aside class="ui left vertical sidebar menu">
		<?php echo $this->element('page/sidebar')?>
	</aside>
	<div id="content" class="pusher">
		<?= $this->Flash->render() ?>
		<?= $this->fetch('content') ?>
	</div>
</div>
</body>
</html>
